<?php

// load public config
require "../config/config.php";
require "Config.php";
require "util/Auth.php";

function __autoload($class){
    require Config::get("paths/libs") . $class . ".php";
}

$bootstrap = new Bootstrap();




$bootstrap->init();


/* WIP

$routes = new Router();

$routes->add("/", "index@Index");
$routes->add("/assist", "help@Index");
$routes->add("/test", function(){echo("test");});

$routes->submit();

*/