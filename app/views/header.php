<!doctype html>
<html>
<head>
    <title><?php echo $this->title; ?></title>
    <link rel="Stylesheet" href="<?php echo Config::get("paths/url"); ?>public/css/default.css" />
    <script type="text/javascript" src="<?php echo Config::get("paths/url") . Config::get("paths/sourceLocation") .  "libs/javascript/core.js"; ?>"></script>
    <script type="text/javascript" src="<?php echo Config::get("paths/url") . Config::get("paths/sourceLocation") .  "libs/javascript/xhr.js"; ?>"></script>
    <?php
    if(isset($this->js)) {
        foreach($this->js as $js) {
            echo "<script type=\"text/javascript\" src=\"". Config::get("paths/url") . Config::get("paths/sourceLocation") . Config::get("paths/views") . "/" . $js . "\"></script>";
        }

    }
    ?>
</head>
<body>
<div class="container">
<div id="header">
    <h1><?php echo $this->heading; ?></h1>
    <a href="<?php echo Config::get("paths/url"); ?>">Home</a>
    <a href="<?php echo Config::get("paths/url"); ?>help">Help</a>
    <?php if(Session::get('loggedIn')==true):?>
        <a href="<?php echo Config::get("paths/url"); ?>dashboard">Dashboard</a>
        <a href="<?php echo Config::get("paths/url"); ?>note">Note</a>
        <?php if(Session::get('role')==owner):?>
            <a href="<?php echo Config::get("paths/url"); ?>user">User</a>
        <?php endif; ?>
        <a href="<?php echo Config::get("paths/url"); ?>dashboard/logout" style="float:right;">Logout</a>
    <?php else: ?>
        <a href="<?php echo Config::get("paths/url"); ?>login" style="float:right;">Login</a>
    <?php endif; ?>
</div>
