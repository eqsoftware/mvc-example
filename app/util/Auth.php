<?php

/**
 * Class Auth
 */
class Auth {
    /**
     *
     */
    public static function handleAuth(){
        $logged = Session::get("loggedIn");
        if($logged == false){
            Session::destroy();
            header("location: ".Config::get("paths/url")."login");
            exit;
        }
    }
}