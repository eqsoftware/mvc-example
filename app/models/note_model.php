<?php

/**
 * Class Note_Model
 */
class Note_Model extends Model
{
    /**
     * Note_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     */
    public function noteList()
    {
        return $this->db->select("notes", array("userId" => Session::get("userId")));
    }

    /**
     * @param $data
     */
    public function create($data){
        $this->db->insert("notes", array(
            "title" => $data['title'],
            "content" => $data['content'],
            "userId" => Session::get("userId"),
            "dateAdded" => date("Y-m-d H:i:s")
        ));
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id){
        return $this->db->delete("notes", "`id` = " . $id . " AND `userId` = " . Session::get("userId"));
    }

    /**
     * @param $id
     * @return array
     */
    public function listNoteById($id){
        return $this->db->select("notes", array("userId" => Session::get("userId"), "id" => $id));
    }

    /**
     * @param $data
     */
    public function editSave($data){
        $this->db->update("notes", array(
            "title" => $data['title'],
            "content" => $data['content']
        ), "`id` = " . $data['id'] . " AND `userId` = " . Session::get("userId"));
    }
}