<?php

/**
 * Class Note
 */
class Note extends Controller
{
    /**
     * Note constructor.
     */
    public function __construct()
    {
        parent::__construct();
        Auth::handleAuth();
        /*
        $logged = Session::get("loggedIn");
        $role = Session::get("role");

        if($logged == false || $role != "owner"){
            Session::destroy();
            header("location: ".URL."login");
            exit;
        }
        */
    }

    /**
     *
     */
    public function index(){
        $this->view->heading = "Notes";
        $this->view->title = "Admin | Notes";
        $this->view->noteList = $this->model->noteList();

        $this->view->render("header");
        $this->view->render("note/index");
        $this->view->render("footer");
    }

    public function add()
    {
        $this->view->heading = "Notes - Add";
        $this->view->title = "Admin | Notes - Add";
        $this->view->action = "create";
        $this->view->buttonAction = "Add";

        $this->view->render("header");
        $this->view->render("note/form");
        $this->view->render("footer");
    }
    /**
     *
     */
    public function create(){
        $data['title'] = $_POST['title'];
        $data['content'] = $_POST['content'];
        $data['dateAdded'] = '';

        //TODO error checking

        $this->model->create($data);
        header("location: " . Config::get("paths/url") . "note");
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        $this->view->title = "Admin | Notes - Edit";
        $this->view->heading = "Notes - Edit";
        $this->view->action = "editSave";
        $this->view->buttonAction = "Update";

        $this->view->note = $this->model->listNoteById($id)[0];

        $this->view->render("header");
        $this->view->render("note/form");
        $this->view->render("footer");
    }

    /**
     * @param $id
     */
    public function editSave($id)
    {
        $data['title'] = $_POST['title'];
        $data['content'] = $_POST['content'];
        $data['id'] = $id;

        //TODO error checking

        $this->model->editSave($data, $id);
        header("location: " . Config::get("paths/url") . "note");
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->model->delete($id);
        header("location: " . Config::get("paths/url") . "note");
    }
}