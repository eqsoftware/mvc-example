<?php

/**
 * Class Login
 */
class Login extends Controller
{
    /**
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->view->heading = "Login";
        $this->view->title = "Admin | Login";
    }

    /**
     *
     */
    public function index()
    {
        $this->view->render("header");
        $this->view->render("login/index");
        $this->view->render("footer");
    }

    /**
     *
     */
    public function loginDo()
    {
        $this->model->loginDo();

    }

}