<?php

require "Form/Validator.php";

//TODO Sanitization

/**
 * Class Form
 */
class Form
{
    /**
     * @var array
     */
    private $_postData = array();
    /**
     * @var null
     */
    private $_currentItem = null;
    /**
     * @var array|Validator
     */
    private $_val = array();
    /**
     * @var array
     */
    private $_error = array();

    /**
     * @param string $type
     * @return bool
     */
    public static function exists($type = "post")
    {
        switch($type){
            case 'post':
                return (!empty($_POST)) ? true : false;
                break;
            case 'get':
                return (!empty($_GET)) ? true : false;
                break;
            default:
                return false;
            break;
        }
    }

    /**
     * @param $item
     * @return string
     */
    public static function get($item)
    {
        if(isset($_POST[$item])){
            return $_POST[$item];
        } else if(isset($_GET[$item])){
            return $_GET[$item];
        } else {
            return '';
        }
    }


    /**
     * Form constructor.
     */
    public function __construct()
    {
        $this->_val = new Validator();
    }

    /**
     * @param $field
     * @return $this
     */
    public function post($field)
    {
        $this->_postData[$field] = $_POST[$field];
        $this->_currentItem = $field;
        return $this;
    }

    /**
     * @param $typeOfValidator
     * @param null $arg
     * @return $this
     */
    public function validate($typeOfValidator, $arg = null)
    {
        if($arg == null){
            $error = $this->_val->{$typeOfValidator}($this->_postData[$this->_currentItem], $this->_currentItem);
        } else {
            $error = $this->_val->{$typeOfValidator}($this->_postData[$this->_currentItem], $this->_currentItem, $arg);
        }

        if($error){
            $this->_error[$this->_currentItem] = $error;
        }

        return $this;
    }

    /**
     * @param bool|false $fieldName
     * @return array|bool
     */
    public function fetch($fieldName = false){
        if($fieldName){
            if(isset($this->_postData[$fieldName])){
                return $this->_postData[$fieldName];
            } else {
                return false;
            }

        } else {
            return $this->_postData;
        }

    }

    /**
     * @return bool
     * @throws Exception
     */
    public function submit()
    {
        if(empty($this->_error)){
            return true;
        } else {
            $str = '';
            foreach ($this->_error as $key => $value){
                $str .= $key . " => " . $value . "\n";
            }
            throw new Exception(json_encode($this->_error));
        }
    }

 }