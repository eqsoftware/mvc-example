<?php

/**
 * Class Token
 */
class Token
{
    /**
     * @return mixed
     */
    public static function generate()
    {
        return Session::set(Config::get("csrf/tokenName"), md5(uniqid()));
    }

    /**
     * @param $token
     * @return bool
     */
    public static function check($token)
    {
        $tokenName = Config::get("csrf/tokenName");
        if(Session::exists($tokenName) && $token === Session::get($tokenName)){
            Session::delete($tokenName);
            return true;
        }
        return false;
    }
}