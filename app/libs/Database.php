<?php

/**
 * Class Database
 */
class Database extends PDO
{
    /**
     * @var null
     */
    private static $_instance = null;

    /**
     * Database constructor.
     */
    function __construct()
    {
        parent::__construct(Config::get("database/type") .
            ':host=' . Config::get("database/host") .
            ';port=' . Config::get("database/port") .
            ';dbname=' . Config::get("database/name").
            ';charset=utf8',
            Config::get("database/username"),
            Config::get("database/password"));
        parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        parent::setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    /**
     * @return Database|null
     */
    public static function getInstance()
    {
        if(!isset(self::$_instance)){
            self::$_instance = new Database();
        }
        return self::$_instance;
    }

    /**
     * @param $table
     * @param array $array
     * @param int $fetchMode
     * @return array
     */
    public function select($table, $array = [], $fetchMode = PDO::FETCH_ASSOC)
    {
        $fieldDetails = [];
        foreach($array as $key => $value){
            $fieldDetails[]= "`".$key."`=:".$key."";
        }
        $sth = $this->prepare("SELECT * FROM " . $table . (count($fieldDetails)>0?" WHERE ".implode(" AND ", $fieldDetails):""));
        foreach($array as $key => $value) {
            $sth->bindValue(":".$key, $value);
        }
        $sth->execute();
        return $sth->fetchAll($fetchMode);
    }

    /**
     * @param $table
     * @param $where
     * @param int $limit
     * @return int
     */
    public function delete($table, $where, $limit = 1)
    {
        return $this->exec("DELETE FROM " . $table . " WHERE " . $where . " LIMIT " . $limit);
    }

    /**
     * @param $table
     * @param $data
     */
    public function insert($table, $data)
    {
        ksort($data);
        $fieldNames = implode("`, `", array_keys($data));
        $fieldValues = ":" . implode(", :", array_keys($data));

        $sth = $this->prepare("INSERT INTO " . $table . " (`".$fieldNames."`) VALUES ($fieldValues)");
        foreach($data as $key => $value) {
            $sth->bindValue(":".$key, $value);
        }
        $sth->execute();
    }

    /**
     * @param $table
     * @param $data
     * @param $where
     */
    public function update($table, $data, $where)
    {
        ksort($data);

        $fieldDetails = NULL;
        foreach($data as $key => $value){
            $fieldDetails .= "`".$key."`=:".$key.",";
        }
        $fieldDetails = rtrim($fieldDetails, ",");
        $sth = $this->prepare("UPDATE " . $table . " SET $fieldDetails WHERE " . $where);
        foreach($data as $key => $value) {
            $sth->bindValue(":".$key, $value);
        }
        $sth->execute();
    }
 }