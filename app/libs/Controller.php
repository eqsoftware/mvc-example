<?php

/**
 * Class Controller
 */
class Controller
{
    /**
     * Controller constructor.
     */
    function __construct()
    {
        Session::init();
        $this->view = new View();
    }

    /**
     * @param $name
     * @param $modelPath
     */
    public function loadModel($name, $modelPath)
    {
        $path = $modelPath . $name . "_model.php";
        if (file_exists($path)) {
            require $path;
            $modelName = $name."_Model";
            $this->model = new $modelName();
        }
    }
      
 }