<?php

/**
 * Class View
 */
class View {
    /**
     * View constructor.
     */
    function __construct()
    {
    }

    /**
     * @param $name
     */
    public function render($name)
    {
        require Config::get("paths/views") . $name . ".php";
    }

    /**
     * @return string
     */
    public static function renderTime()
    {
        return number_format(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 4);
    }
}