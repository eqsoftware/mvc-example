<?
$GLOBALS['config'] = array(
    "general" => array(
        "company"           => "NeoFuture",
        "version"           => "0.0.1"
    ),
    "database"  => array(
        "type"              => "mysql",
        "host"              => "127.0.0.1",
        "name"              => "mvc",
        "username"          => "root",
        "password"          => "yoshi355466",
        "port"              => "3306"
    ),
    "paths" => array(
        "url"               => "//mvc/",
        "sourceLocation"    => "app/",
        "libs"              => "libs/",
        "controllers"       => "controllers/",
        "views"             => "views/",
        "models"            => "models/",
        "errorFile"         => "error.php"
    ),
    "controllers" => array(
        "defaultController" => "index.php",
        "defaultMethod"     => "Index"
    ),
    "hashing" => array(
        "algorithm"         => "sha256",
        "passwordSalt"      => "MixedUpKeyFromAGalaxyFarFarAway",
        "generalSalt"       => "Yo!"

    ),
    "csrf"  => array(
        "tokenName"         => "csrfToken"
    )
);