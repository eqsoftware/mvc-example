-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2015 at 02:31 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `id` int(200) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `text`) VALUES
(12, 'werner'),
(14, 'erererer'),
(36, 'dsfdsfsdf'),
(40, 'dsfdsfsdf'),
(42, 'dsfdsfsdfsfsdf'),
(43, 'dsfdsfsdfsfsdf'),
(44, 'dsfdsfsdfsfsdfsdfsdf'),
(45, 'User Database'),
(46, 'wewewefwefewf'),
(47, 'wewewefwefewf'),
(49, 'wrwerwerwrwer');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(200) NOT NULL,
  `userId` int(200) NOT NULL,
  `title` varchar(400) NOT NULL,
  `content` longtext NOT NULL,
  `dateAdded` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `userId`, `title`, `content`, `dateAdded`) VALUES
(1, 1, 'dsfsdffsd', 'sdfsdfsdfsdfsdfsf def sdfksdfjhsidufuhi u isdfu hisdfh iusgf iugsdiugs difug fiugs ifugsdifugsdiufg isudgf iusgd iug igus ifug iug sdig siudfg iug iusgd iugs difug iug isudgf isudfg iug iug isudfg isudfg ', '0000-00-00 00:00:00'),
(2, 2, '123', '123123123', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(200) NOT NULL,
  `login` varchar(25) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role` enum('default','admin','owner') NOT NULL DEFAULT 'default'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `role`) VALUES
(1, 'neofuture', '471423c9bc5fd36cbb20901f708038f801287a2c5784434f4ccd70a94dc84cae', 'owner'),
(2, 'carl', '471423c9bc5fd36cbb20901f708038f801287a2c5784434f4ccd70a94dc84cae', 'admin'),
(7, 'werewr', 'ec63b4ddd065cd25731371dfc5572c68c55b05c1fa10123c17980b2563a38630', 'default');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
